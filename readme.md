![img](SYMFONY/img.png)

[Manual](https://www.digitalocean.com/community/tutorials/how-to-deploy-a-symfony-application-to-production-on-ubuntu-14-04)

#ERRORS
* A l'hora d'instal·lar el php5-cli / php5-curl hem de posar php-cli i php-curl ja que la verisó 5 està desfasada.
* La ruta /etc/mysql/my.cnf es incorrecte, actualment es troba a la ruta /etc/mysql/mysql.conf.d/mysqld.cnf
* Quan es crea el virtualhost està utilitzant les directives antigues, actualment s'utilitza "Require all granted"
* El timezone en el manual s'especifica Europe/Amsterdam pero nosaltres hem de posar Europe/Madrid
